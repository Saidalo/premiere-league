<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlMatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("pl_matches", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("week_num");
            $table->string("team1");
            $table->integer("team1_result")->nullable();
            $table->string("team2");
            $table->integer("team2_result")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("pl_matches");
    }
}
