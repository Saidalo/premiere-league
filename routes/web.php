<?php
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\MatchesController;
use App\Http\Controllers\SetupController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [SetupController::class, 'setup']);
Route::get('/week/{week_num}', [IndexController::class, 'index']);
Route::get('/play-all-matches/results/{max_weeks}', [IndexController::class, 'showAllResults']);
Route::get('/play-all-matches/{week_num}', [IndexController::class, 'playAllMatches']);

Route::post('/populate-db', [AjaxController::class, 'populate']);
Route::post('/arrange-matches', [AjaxController::class, 'arrangeMatches']);