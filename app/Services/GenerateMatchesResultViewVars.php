<?php

namespace App\Services;

use App\Models\Clubs;
use App\Models\ClubsByWeeks;
use App\Models\Matches;

class GenerateMatchesResultViewVars
{
    protected $array_of_vars;

    public function __construct($request, $club_num)
    {
        $total_num_of_weeks = $request->max_weeks;
        $ordinal_suffixes = [];
        for ($i = 1; $i <= $total_num_of_weeks; $i++) {
            array_push($ordinal_suffixes, ordinalOf($i));
        }

        $clubs_by_weeks = [];
        $all_matches = [];
        for ($i = 1; $i <= $total_num_of_weeks; $i++) {
            $record = ClubsByWeeks::where("week_num", $i)
                ->get()
                ->toArray();
            $clubs_by_weeks[$i - 1] = $record;
        }
        for ($i = 1; $i <= $total_num_of_weeks; $i++) {
            $record = Matches::where("week_num", $i)
                ->get()
                ->toArray();
            $all_matches[$i - 1] = $record;
        }
        $percentages = new CalculateWinPercentages();
        $club_names = Clubs::pluck("name")->toArray();
        $this->array_of_vars = [
            "clubs_by_weeks" => $clubs_by_weeks,
            "matches" => $all_matches,
            "percentages" => $percentages->get(),
            "matches_per_week" => $club_num / 2,
            "club_names" => $club_names,
            "ordinal_suffixes" => $ordinal_suffixes,
        ];
    }

    public function get() {
        return $this->array_of_vars;
    }
}