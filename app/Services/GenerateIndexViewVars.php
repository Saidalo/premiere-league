<?php

namespace App\Services;

use App\Models\Clubs;
use App\Models\ClubsByWeeks;
use App\Models\Matches;

use App\Services\CalculateWinPercentages;
use App\Services\PlayMatch;
use App\Services\UpdateClubsModel;

class GenerateIndexViewVars
{
    protected $array_of_vars;

    public function __construct($request, $club_num)
    {
        $requestPath = $request->path();
        if (strpos($requestPath, "end")) {
            $msg = "Matches are all played!";
            $current_week = 1;
        } else {
            $msg = false;
            $current_week = $request->week_num;
        }
        $weekExists = ClubsByWeeks::where("week_num", $current_week)
            ->get()
            ->isNotEmpty();

        $matches_per_week = $club_num / 2;
        $current_week_matches = Matches::where("week_num", $current_week)
            ->get()
            ->toArray();
        
        foreach ($current_week_matches as $key => $match) {
            if (
                $match["team1_result"] === null &&
                $match["team2_result"] === null
            ) {
                $play_match = new PlayMatch(
                    $match["team1"],
                    $match["team2"]
                );
                $match_outcome = $play_match->get();
                $current_week_matches[$key]["team1_result"] =
                    $match_outcome["team1"]["result"];
                $current_week_matches[$key]["team2_result"] =
                    $match_outcome["team2"]["result"];

                updateClubsModel($match_outcome);

                Matches::where("week_num", $match["week_num"])
                    ->where("team1", $match["team1"])
                    ->update([
                        "team1_result" => $match_outcome["team1"]["result"],
                        "team2_result" => $match_outcome["team2"]["result"],
                    ]);
            }
        }

        $arrayOfRecords_currentWeek = [];
        if (!$weekExists) {
            // rearranging teams in table
            $arrayOfRecords_allWeeks = [];
            $orderedByTGandTP = Clubs::all();
            $orderedByTGandTP = $orderedByTGandTP
                ->sortByDesc("GD")
                ->sortByDesc("total_points");
            Clubs::truncate();

            foreach ($orderedByTGandTP as $club) {
                $club = $club->toArray();
                unset($club["club_id"]);
                array_push($arrayOfRecords_currentWeek, $club);
                $club["week_num"] = $current_week;
                array_push($arrayOfRecords_allWeeks, $club);
            }
            Clubs::insert($arrayOfRecords_currentWeek);
            ClubsByWeeks::insert($arrayOfRecords_allWeeks);
        } else {
            $fromDB = ClubsByWeeks::where("week_num", $current_week)->get();
            Clubs::truncate();

            foreach ($fromDB as $club) {
                $club = $club->toArray();
                unset($club["id"]);
                unset($club["week_num"]);
                array_push($arrayOfRecords_currentWeek, $club);
            }
            Clubs::insert($arrayOfRecords_currentWeek);
        }

        $calculated_percentages = new CalculateWinPercentages();
        $ordinal_suffix = ordinalOf($current_week);

        $this->array_of_vars = [
            "clubs" => Clubs::all(),
            "current_week" => $current_week,
            "matches" => $current_week_matches,
            "matches_per_week" => $matches_per_week,
            "msg" => $msg,
            "percentages" => $calculated_percentages->get(),
            "playAll" => $request->playAll,
            "ordinal_suffix" => $ordinal_suffix,
        ];
    }

    public function get()
    {
        return $this->array_of_vars;
    }
}