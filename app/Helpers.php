<?php

use App\Models\Clubs;
use Illuminate\Support\Facades\Auth;

if (!function_exists('ordinalOf')) {
    function ordinalOf($number)
    {
        $ends = ["th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"];
        if ($number % 100 >= 11 && $number % 100 <= 13) {
            return "th";
        } else {
            return $ends[$number % 10];
        }
    }
}

if (!function_exists("makeSeed")) {
    function makeSeed()
    {
        list($usec, $sec) = explode(" ", microtime());
        return $sec + $usec * 2;
    }
}

if (!function_exists("makeMatchCombinations")) {
    function makeMatchCombinations ($club_names)
    {
        $resonse_array = [];
        for ($i = 0; $i < sizeof($club_names); $i++) {
            for ($j = 0; $j < sizeof($club_names); $j++) {
                if ($club_names[$i] !== $club_names[$j]) {
                     array_push($resonse_array, [
                        $club_names[$i] => $club_names[$j],
                    ]);
                 }
            }
        }
        return $resonse_array;
    }
}

if(!function_exists("updateClubsModel")) {
    function updateClubsModel($match_outcome)
    {
        foreach ($match_outcome as $playedTeam) {
            Clubs::where("name", $playedTeam["name"])->update([
                "total_points" => $playedTeam["total_points"],
                "played" => $playedTeam["played"],
                "won" => $playedTeam["won"],
                "drown" => $playedTeam["drown"],
                "lost" => $playedTeam["lost"],
                "GD" => $playedTeam["GD"],
            ]);
        }
    }
}