<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Play All Matches</title>
	<link rel="stylesheet" type="text/css" href="../../css/index.css">
    <link rel="stylesheet" type="text/css" href="../../css/all-played.css">
</head>
<body>
	<div class="main">
		<div class="center">
            <div class="all-weeks-statistics">
                @foreach ($clubs_by_weeks as $week_key => $week)
                <div class="single-week-statistics">
                    <div class="week-header">
                        <h2><em>Week {{$week_key + 1}}</em></h2>
                    </div>
                    <div class="statistics-container">
                        <table class="statistics">
                            <thead>
                                <tr>
                                    <th class="global-header" colspan="7">League Table</th>
                                    <th class="global-header" colspan="3">Match Results</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <th class="cell teams">Teams</th>
                                    <th class="cell">PTS</th>
                                    <th class="cell">P</th>
                                    <th class="cell">W</th>
                                    <th class="cell">D</th>
                                    <th class="cell">L</th>
                                    <th class="cell">GD</th>
                                    <th colspan="3">{{$week_key + 1}}<sup>{{$ordinal_suffixes[$week_key]}}</sup> Week Match Results</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($week as $key => $club)
                                <tr>
                                    <td class="cell teams">{{$club["name"]}}</td>
                                    <td class="cell">{{$club["total_points"]}}</td>
                                    <td class="cell">{{$club["played"]}}</td>
                                    <td class="cell">{{$club["won"]}}</td>
                                    <td class="cell">{{$club["drown"]}}</td>
                                    <td class="cell">{{$club["lost"]}}</td>
                                    <td class="cell">{{$club["GD"]}}</td>
                                    @if ($key < $matches_per_week)
                                    <td class = "no-borders to-left">{{$matches[$week_key][$key]["team1"]}}</td>
                                    <td class = "no-borders result">{{$matches[$week_key][$key]["team1_result"] . " - " . $matches[$week_key][$key]["team2_result"]}}</td>
                                    <td class = "no-borders to-right">{{$matches[$week_key][$key]["team2"]}}</td>
                                    @else
                                    <td class = "no-borders"></td>
                                    <td class = "no-borders"></td>
                                    <td class = "no-borders"></td>
                                    @endif
                                </tr>
                                @endforeach
                                
                            </tbody>	
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
            
            
            <div>
                <div class="week-header">
                    <h2><em>Predictions</em></h2>
                </div>
                <div class="predictions-container">
                    <table class="predictions">
                        <tr>
                            <th class="secondary-header" colspan="2">Predictions of Championship</th>
                        </tr>
                        @foreach($club_names as $key => $club_name)
                        <tr>
                            <td class="teams">{{$club_name}}</td>
                            <td>%{{$percentages[$key]}}</td>
                        </tr>
                        @endforeach
                    </table>
			</div>
            </div>
			
			
		</div>
	</div>
</body>
</html>